package ru.ekfedorov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ekfedorov.tm.api.repository.dto.IProjectRepository;
import ru.ekfedorov.tm.api.service.dto.IProjectService;
import ru.ekfedorov.tm.dto.Project;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.empty.*;
import ru.ekfedorov.tm.exception.incorrect.IncorrectIndexException;
import ru.ekfedorov.tm.exception.system.NullComparatorException;
import ru.ekfedorov.tm.exception.system.NullObjectException;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.ekfedorov.tm.util.ValidateUtil.*;

@Service
public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    public IProjectRepository getRepository() {
        return context.getBean(IProjectRepository.class);
    }

    @Override
    @SneakyThrows
    public void add(@Nullable final Project entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.add(entity);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<Project> entities) {
        if (entities == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            entities.forEach(projectRepository::add);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.clear();
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean contains(@NotNull final String id) {
        @NotNull final IProjectRepository projectRepository = getRepository();
        if (isEmpty(id)) throw new IdIsEmptyException();
        try {
            return projectRepository.findOneById(id).isPresent();
        } finally {
            projectRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            return projectRepository.findAll();
        } finally {
            projectRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAllSort(
            @Nullable final String sort
    ) {
        @NotNull final IProjectRepository projectRepository = getRepository();
        if (isEmpty(sort)) throw new NullComparatorException();
        try {
            @Nullable Sort sortType = Sort.valueOf(sort);
            @NotNull final Comparator<Project> comparator = sortType.getComparator();
            return projectRepository.findAll().stream().sorted(comparator).collect(Collectors.toList());
        } finally {
            projectRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Project> findOneById(
            @Nullable final String id
    ) {
        @NotNull final IProjectRepository projectRepository = getRepository();
        if (isEmpty(id)) throw new IdIsEmptyException();
        try {
            return projectRepository.findOneById(id);
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new LoginIsEmptyException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.removeOneById(id);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public Project add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        if (isEmpty(description)) throw new DescriptionIsEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.add(project);
            projectRepository.commit();
            return project;
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        if (status == null) throw new NullObjectException();
        @NotNull final Optional<Project> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity.get());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void clear(@Nullable final String userId) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.clearByUserId(userId);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        @NotNull final IProjectRepository projectRepository = getRepository();
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        try {
            return projectRepository.findAllByUserId(userId);
        } finally {
            projectRepository.close();
        }
    }


    @SneakyThrows
    @NotNull
    @Override
    public Optional<Project> findOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        if (isEmpty(id)) throw new IdIsEmptyException();
        try {
            return projectRepository.findOneByIdAndUserId(userId, id);
        } finally {
            projectRepository.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Optional<Project> findOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        try {
            return projectRepository.findOneByIndex(userId, index);
        } finally {
            projectRepository.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public Optional<Project> findOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        if (isEmpty(name)) throw new NameIsEmptyException();
        try {
            return projectRepository.findOneByName(userId, name);
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final Project entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.removeOneById(entity.getId());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void remove(
            @Nullable final String userId, @Nullable final Project entity
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.removeOneByIdAndUserId(userId, entity.getId());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.removeOneByIdAndUserId(userId, id);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            @NotNull Optional<Project> project = projectRepository.findOneByIndex(userId, index);
            if (!project.isPresent()) throw new UserNotFoundException();
            projectRepository.removeOneByIdAndUserId(userId, project.get().getId());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.removeOneByName(userId, name);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final Optional<Project> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity.get());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        if (status == null) throw new NullObjectException();
        @NotNull final Optional<Project> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity.get());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        if (status == null) throw new NullObjectException();
        @NotNull final Optional<Project> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity.get());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public @NotNull List<Project> findAll(@Nullable String userId, @Nullable String sort) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        if (isEmpty(sort)) throw new NullComparatorException();
        try {
            @Nullable Sort sortType = Sort.valueOf(sort);
            @NotNull final Comparator<Project> comparator = sortType.getComparator();
            return projectRepository
                    .findAllByUserId(userId)
                    .stream()
                    .sorted(comparator)
                    .collect(Collectors.toList());
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final Optional<Project> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity.get());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        @NotNull final Optional<Project> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity.get());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final Optional<Project> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity.get());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void startById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final Optional<Project> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity.get());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void startByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        @NotNull final Optional<Project> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity.get());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void startByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) return;
        @NotNull final Optional<Project> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity.get());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final Optional<Project> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity.get());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

}
