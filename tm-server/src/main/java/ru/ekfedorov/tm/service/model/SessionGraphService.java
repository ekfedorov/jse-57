package ru.ekfedorov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ekfedorov.tm.api.repository.model.ISessionGraphRepository;
import ru.ekfedorov.tm.api.service.IPropertyService;
import ru.ekfedorov.tm.api.service.model.ISessionGraphService;
import ru.ekfedorov.tm.api.service.model.IUserGraphService;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.exception.empty.IdIsEmptyException;
import ru.ekfedorov.tm.exception.empty.LoginIsEmptyException;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;
import ru.ekfedorov.tm.exception.system.NullComparatorException;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.exception.system.UserIsLockedException;
import ru.ekfedorov.tm.model.SessionGraph;
import ru.ekfedorov.tm.model.UserGraph;
import ru.ekfedorov.tm.util.HashUtil;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

@Service
public final class SessionGraphService extends AbstractGraphService<SessionGraph> implements ISessionGraphService {

    @NotNull
    public ISessionGraphRepository getRepository() {
        return context.getBean(ISessionGraphRepository.class);
    }

    @NotNull
    @Autowired
    private IUserGraphService userGraphService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Override
    @SneakyThrows
    public void add(@Nullable final SessionGraph session) {
        if (session == null) throw new NullObjectException();
        @NotNull final ISessionGraphRepository sessionRepository = getRepository();
        try {
            sessionRepository.begin();
            sessionRepository.add(session);
            sessionRepository.commit();
        } catch (@NotNull final Exception e) {
            sessionRepository.rollback();
            throw e;
        } finally {
            sessionRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<SessionGraph> entities) {
        if (entities == null) throw new NullObjectException();
        @NotNull final ISessionGraphRepository sessionRepository = getRepository();
        try {
            sessionRepository.begin();
            entities.forEach(sessionRepository::add);
            sessionRepository.commit();
        } catch (@NotNull final Exception e) {
            sessionRepository.rollback();
            throw e;
        } finally {
            sessionRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final ISessionGraphRepository sessionRepository = getRepository();
        try {
            sessionRepository.begin();
            sessionRepository.clear();
            sessionRepository.commit();
        } catch (@NotNull final Exception e) {
            sessionRepository.rollback();
            throw e;
        } finally {
            sessionRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean contains(@NotNull final String id) {
        @NotNull final ISessionGraphRepository sessionRepository = getRepository();
        if (isEmpty(id)) throw new IdIsEmptyException();
        try {
            return sessionRepository.findOneById(id).isPresent();
        } finally {
            sessionRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionGraph> findAll() {
        @NotNull final ISessionGraphRepository sessionRepository = getRepository();
        try {
            return sessionRepository.findAll();
        } finally {
            sessionRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionGraph> findAllSort(
            @Nullable final String sort
    ) {
        @NotNull final ISessionGraphRepository sessionRepository = getRepository();
        if (isEmpty(sort)) throw new NullComparatorException();
        try {
            @Nullable Sort sortType = Sort.valueOf(sort);
            @NotNull final Comparator<SessionGraph> comparator = sortType.getComparator();
            return sessionRepository.findAll().stream().sorted(comparator).collect(Collectors.toList());
        } finally {
            sessionRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<SessionGraph> findOneById(
            @Nullable final String id
    ) {
        @NotNull final ISessionGraphRepository sessionRepository = getRepository();
        if (isEmpty(id)) throw new IdIsEmptyException();
        try {
            return sessionRepository.findOneById(id);
        } finally {
            sessionRepository.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new LoginIsEmptyException();
        @NotNull final ISessionGraphRepository sessionRepository = getRepository();
        try {
            sessionRepository.begin();
            sessionRepository.removeOneById(id);
            sessionRepository.commit();
        } catch (@NotNull final Exception e) {
            sessionRepository.rollback();
            throw e;
        } finally {
            sessionRepository.close();
        }
    }

    @SneakyThrows
    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (isEmpty(login) || isEmpty(password)) return false;
        final @NotNull Optional<UserGraph> user = userGraphService.findByLogin(login);
        if (!user.isPresent()) return false;
        if (user.get().isLocked()) throw new UserIsLockedException();
        final String passwordHash = HashUtil.salt(propertyService, password);
        if (isEmpty(passwordHash)) return false;
        return passwordHash.equals(user.get().getPasswordHash());
    }

    @Override
    @Nullable
    @SneakyThrows
    public SessionGraph close(@Nullable final SessionGraph session) {
        if (session == null) return null;
        @NotNull final ISessionGraphRepository sessionRepository = getRepository();
        try {
            sessionRepository.begin();
            sessionRepository.removeOneById(session.getId());
            sessionRepository.commit();
            return session;
        } catch (@NotNull final Exception e) {
            sessionRepository.rollback();
            throw e;
        } finally {
            sessionRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionGraph open(
            @Nullable final String login,
            @Nullable final String password
    ) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        final @NotNull Optional<UserGraph> user = userGraphService.findByLogin(login);
        if (!user.isPresent()) return null;
        @NotNull final SessionGraph session = new SessionGraph();
        session.setUser(user.get());
        @Nullable final SessionGraph signSession = sign(session);
        if (signSession == null) return null;
        @NotNull final ISessionGraphRepository sessionRepository = getRepository();
        try {
            sessionRepository.begin();
            sessionRepository.add(signSession);
            sessionRepository.commit();
            return signSession;
        } catch (@NotNull final Exception e) {
            sessionRepository.rollback();
            throw e;
        } finally {
            sessionRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final SessionGraph entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final ISessionGraphRepository sessionRepository = getRepository();
        try {
            sessionRepository.begin();
            sessionRepository.removeOneById(entity.getId());
            sessionRepository.commit();
        } catch (@NotNull final Exception e) {
            sessionRepository.rollback();
            throw e;
        } finally {
            sessionRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionGraph session) {
        if (session == null) throw new AccessDeniedException();
        if (isEmpty(session.getSignature())) throw new AccessDeniedException();
        if (session.getUser() == null) throw new AccessDeniedException();
        @Nullable final SessionGraph temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final SessionGraph sessionTarget = sign(temp);
        if (sessionTarget == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionTarget.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        @NotNull final ISessionGraphRepository sessionRepository = getRepository();
        if (!check) throw new AccessDeniedException();
        try {
            if (!sessionRepository.findOneById(session.getId()).isPresent()) throw new AccessDeniedException();
        } finally {
            sessionRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void validateAdmin(@Nullable final SessionGraph session, @Nullable final Role role) {
        if (session == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        if (isEmpty(session.getUser().getId())) throw new AccessDeniedException();
        validate(session);
        final @NotNull Optional<UserGraph> user = userGraphService.findOneById(session.getUser().getId());
        if (!user.isPresent()) throw new AccessDeniedException();
        if (user.get().getRole() != Role.ADMIN) throw new AccessDeniedException();
    }

    @Nullable
    public SessionGraph sign(@Nullable final SessionGraph session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.salt(propertyService, session);
        session.setSignature(signature);
        return session;
    }

}
