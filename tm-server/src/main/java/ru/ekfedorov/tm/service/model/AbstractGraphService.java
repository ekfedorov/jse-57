package ru.ekfedorov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.ekfedorov.tm.api.service.IConnectionService;
import ru.ekfedorov.tm.api.service.model.IGraphService;
import ru.ekfedorov.tm.model.AbstractGraphEntity;


public abstract class AbstractGraphService<E extends AbstractGraphEntity> implements IGraphService<E> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

}
