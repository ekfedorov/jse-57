package ru.ekfedorov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.ekfedorov.tm.api.service.dto.IService;
import ru.ekfedorov.tm.dto.AbstractEntity;


public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

}
