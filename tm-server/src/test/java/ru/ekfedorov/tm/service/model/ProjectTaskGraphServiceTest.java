package ru.ekfedorov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ekfedorov.tm.api.service.IConnectionService;
import ru.ekfedorov.tm.api.service.model.IProjectGraphService;
import ru.ekfedorov.tm.api.service.model.IProjectTaskGraphService;
import ru.ekfedorov.tm.api.service.model.ITaskGraphService;
import ru.ekfedorov.tm.api.service.model.IUserGraphService;
import ru.ekfedorov.tm.cofiguration.ServerConfiguration;
import ru.ekfedorov.tm.marker.DBCategory;
import ru.ekfedorov.tm.model.ProjectGraph;
import ru.ekfedorov.tm.model.TaskGraph;
import ru.ekfedorov.tm.model.UserGraph;
import ru.ekfedorov.tm.service.TestUtil;

import javax.persistence.EntityManagerFactory;
import java.util.Optional;

public class ProjectTaskGraphServiceTest {

    @NotNull AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private final IProjectGraphService projectService = context.getBean(IProjectGraphService.class);

    @NotNull
    private final IUserGraphService userService = context.getBean(IUserGraphService.class);

    @NotNull
    private final ITaskGraphService taskService = context.getBean(ITaskGraphService.class);

    @NotNull
    private final IProjectTaskGraphService projectTaskService = context.getBean(IProjectTaskGraphService.class);

    {
        TestUtil.initUser();
    }

    @Before
    public void before() {
        context.getBean(EntityManagerFactory.class).createEntityManager();
    }

    @After
    public void after() {
        context.getBean(EntityManagerFactory.class).close();
    }

    @Test
    @Category(DBCategory.class)
    public void bindTaskByProjectIdTest() {
        final TaskGraph task = new TaskGraph();
        final @NotNull Optional<UserGraph> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        final ProjectGraph project = projectService.add(userId, "testBind", "-");
        final String projectId = project.getId();
        final String taskId = task.getId();
        task.setUser(user.get());
        taskService.add(task);
        projectTaskService.bindTaskByProject(userId, projectId, taskId);
        Assert.assertTrue(taskService.findOneById(userId, taskId).isPresent());
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByProjectIdTest() {
        final TaskGraph task = new TaskGraph();
        final @NotNull Optional<UserGraph> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        final ProjectGraph project = projectService.add(userId, "testFindAll", "-");
        final String projectId = project.getId();
        task.setUser(user.get());
        task.setProject(project);
        taskService.add(task);
        Assert.assertFalse(projectTaskService.findAllByProjectId(userId, projectId).isEmpty());
        Assert.assertEquals(1, projectTaskService.findAllByProjectId(userId, projectId).size());

        final TaskGraph task2 = new TaskGraph();
        task2.setUser(user.get());
        task2.setProject(project);
        taskService.add(task2);
        Assert.assertEquals(2, projectTaskService.findAllByProjectId(userId, projectId).size());

        final TaskGraph task3 = new TaskGraph();
        final @NotNull Optional<UserGraph> user2 = userService.findByLogin("test2");
        final String user2Id = user2.get().getId();
        task3.setUser(user2.get());
        task3.setProject(project);
        taskService.add(task3);
        Assert.assertEquals(2, projectTaskService.findAllByProjectId(userId, projectId).size());
        Assert.assertEquals(1, projectTaskService.findAllByProjectId(user2Id, projectId).size());

        final TaskGraph task4 = new TaskGraph();
        final ProjectGraph project2 = projectService.add(userId, "testFindAll2", "-");
        final String project2Id = project2.getId();
        task4.setUser(user.get());
        task4.setProject(project2);
        taskService.add(task4);
        Assert.assertEquals(2, projectTaskService.findAllByProjectId(userId, projectId).size());
        Assert.assertEquals(1, projectTaskService.findAllByProjectId(userId, project2Id).size());
    }

    @Test
    @Category(DBCategory.class)
    public void removeAllByProjectIdTest() {
        final TaskGraph task = new TaskGraph();
        final TaskGraph task2 = new TaskGraph();
        final TaskGraph task3 = new TaskGraph();
        final @NotNull Optional<UserGraph> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        final ProjectGraph project = projectService.add(userId, "testBind", "-");
        final String projectId = project.getId();
        task.setUser(user.get());
        task.setProject(project);
        task2.setUser(user.get());
        task2.setProject(project);
        task3.setUser(user.get());
        task3.setProject(project);
        taskService.add(task);
        taskService.add(task2);
        taskService.add(task3);
        Assert.assertEquals(3, projectTaskService.findAllByProjectId(userId, projectId).size());
        projectTaskService.removeProjectById(userId, projectId);
        Assert.assertTrue(projectTaskService.findAllByProjectId(userId, projectId).isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void unbindTaskFromProjectIdTest() {
        final TaskGraph task = new TaskGraph();
        final @NotNull Optional<UserGraph> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        final String taskId = task.getId();
        task.setUser(user.get());
        taskService.add(task);
        projectTaskService.unbindTaskFromProject(userId, taskId);
        Assert.assertTrue(taskService.findOneById(userId, taskId).isPresent());
        projectTaskService.unbindTaskFromProject(userId, taskId);
        final TaskGraph task2 = taskService.findOneById(userId, taskId).get();
        Assert.assertNull(task2.getProject());
    }

}
